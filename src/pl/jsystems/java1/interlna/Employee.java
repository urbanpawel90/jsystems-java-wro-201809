package pl.jsystems.java1.interlna;

import pl.jsystems.java1.Person;

public class Employee extends Person {
    public Employee(String name, String surname, int age) {
        super(name, surname, age);
    }

    @Override
    public void birthday() {
        super.birthday();
        System.out.println("100 lat!");
    }
}
