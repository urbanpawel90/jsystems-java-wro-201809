package pl.jsystems.java1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exercise3_ReversePrint {
    public static void main(String[] args) {
        try {
            reversePrint();
        } catch(InputMismatchException inputException) {
            // Nie podano liczby!
            System.out.println("Podana wartosc nie jest liczba!");
        } catch(NegativeArraySizeException arraySizeException) {
            // Negatywny rozmiar tablicy
            System.out.println("Podana wielkość próby jest mniejsza od 0!");
        }
    }

    public static void reversePrint() {
        // Zapytaj uzytkownika o N liczb
        // Wypisz je w odwrotnej kolejnosci na ekran
        Scanner sc = new Scanner(System.in);
        // sout -> System.out.println()
        System.out.println("Wpisz ilość liczb:");
        int size = sc.nextInt();
        int[] numbers = new int[size];

        for (int i = 0; i < size; i++) {
            System.out.println("Podaj liczbe " + i);
            numbers[i] = sc.nextInt();
        }

        System.out.println();
        for (int i = size - 1; i >= 0; i--) {
            System.out.println(numbers[i]);
        }
    }
}
