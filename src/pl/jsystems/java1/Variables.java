package pl.jsystems.java1;

import java.util.Arrays;

public class Variables {
    public static void main(String[] args) {
        int number = 257;
        modify(number);

        String[] array = new String[] {"Ala", "kot"};
        modify(array);

        System.out.println(Arrays.toString(array));
        System.out.println(number);
    }

    public static void modify(String[] x) {
        x = new String[] {"Wujek", "Zenek"};
        x[0] = "Zero";
    }

    // Przeciążanie - overloading
    public static void modify(int x) {
        x = 10;
    }
}
