package pl.jsystems.java1;

import java.util.Scanner;

public class JavaConsoleIO {
    public static void main(String[] args) {
        System.out.println("Wprowadz imie:");

        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();

        System.out.println("Hello, " + name);
    }
}
