package pl.jsystems.java1;

import java.util.ArrayList;
import java.util.List;

public class HelloWorld {
    private static List<String> NUMBERS = new ArrayList<>();

    // Inicjalizator statyczny
    static {
        NUMBERS.add("111234");
        NUMBERS.add("111234");
        NUMBERS.add("111234");
    }

    public static void main(String[] args) {
        System.out.println("Kurs Java!");
        for (String arg : args) {
            System.out.println(arg);
        }

        Element obj = new Element();
    }

    private String name;

    public void method() {
        Element el = new Element();
    }

    public static class Element {
        public void m1() {
        }
    }
}
