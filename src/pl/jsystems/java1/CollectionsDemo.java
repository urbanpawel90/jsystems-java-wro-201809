package pl.jsystems.java1;

import pl.jsystems.java1.interlna.Employee;

import java.util.*;

public class CollectionsDemo {
    public static void main(String[] args) {
        List<String> lista = new ArrayList<>();

        Set<String> set = new HashSet<>();
        Set<String> set2 = new LinkedHashSet<>();
        Set<String> tree = new TreeSet<>();

        Map<String, String> map = new HashMap<>();

        Person p1 = new Person("Jan", "KOwalskiego", 20);
        Person p2 = new Employee("Edyta", "Gorniak", 50);

        p1.birthday();
        p2.birthday();

        System.out.println("p1 wiek " + p1.getAge());
        System.out.println("p2 wiek " + p2.getAge());
    }
}
