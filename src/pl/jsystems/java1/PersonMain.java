package pl.jsystems.java1;

public class PersonMain {
    public static void main(String[] args) {
        Person p1 = new Person("Jan", "Kowalski", 35);

        String[] strings = new String[] {"Ala", "ma", "kota"};

        Person p2 = new Person();
        p2.setName("Jan");
        p2.setSurname("Kowalski");
    }
}
