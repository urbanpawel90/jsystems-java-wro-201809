package pl.jsystems.java1;

import java.util.Arrays;
import java.util.Scanner;

public class Exercise2_Anagram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // sout - System.out.println
        System.out.println("Podaj pierwsze slowo");
        String word1 = sc.nextLine();
        System.out.println("Podaj drugie slowo");
        String word2 = sc.nextLine();

        System.out.println(areAnagrams(word1.trim(), word2.trim()));
    }

    public static boolean areAnagrams(String word1, String word2) {
        if (word1.length() != word2.length()) {
            return false;
        }
        int[] counters = new int[256];
        // Count word1 chars
        for (int i = 0; i < word1.length(); i++) {
            char charAt = word1.charAt(i);
            counters[charAt]++;
        }

        for (int i = 0; i < word2.length(); i++) {
            char charAt = word2.charAt(i);
            counters[charAt]--;
        }

        // ponizsze dziala, bo nowa tablica jest wypelniania zerami
        // jezeli counters tez ma same 0, to wynik bedzie true
        return Arrays.equals(counters, new int[256]);
    }
}
