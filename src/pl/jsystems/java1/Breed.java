package pl.jsystems.java1;

public enum Breed {
    DOG("Burek"), CAT("Rudy");

    private String desc;

    Breed(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return this.desc;
    }
}
