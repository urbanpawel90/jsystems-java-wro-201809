package pl.jsystems.java1.interfaces;

public class IngAtm implements Atm, DepositMachine {
    private double amount;

    public IngAtm(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean withdraw(double amount) {
        if (amount > this.amount) {
            return false;
        }
        this.amount -= amount;
        return true;
    }

    @Override
    public double getAccountBalance() {
        return amount;
    }

    @Override
    public void deposit(double amount) {
        if (amount >= 0) {
            this.amount += amount;
        }
    }
}
