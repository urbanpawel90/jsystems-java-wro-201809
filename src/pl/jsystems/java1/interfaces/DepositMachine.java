package pl.jsystems.java1.interfaces;

public interface DepositMachine {
    void deposit(double amount);
}
