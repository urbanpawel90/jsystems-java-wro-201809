package pl.jsystems.java1.interfaces;

public class BankClient {
    public static void main(String[] args) {
        IngAtm ing = new IngAtm(100);
        testBank(ing, ing);
    }

    public static void testBank(Atm atm,
                                DepositMachine deposit) {
        System.out.println(atm.getAccountBalance());
        atm.withdraw(20.0d);
//        atm.withdraw(120.0d);
        deposit.deposit(50.0d);
        System.out.println(atm.getAccountBalance());
    }
}
