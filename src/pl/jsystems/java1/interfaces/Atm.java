package pl.jsystems.java1.interfaces;

public interface Atm {
    boolean withdraw(double amount);

    double getAccountBalance();
}
