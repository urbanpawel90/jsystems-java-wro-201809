package pl.jsystems.java1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ExceptionDemo {
    public static void main(String[] args) {
        try {
            readFile();
        } catch (NegativeArraySizeException ex) {
            ex.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFile() throws IOException {
        // ...
        Files.copy(Paths.get("./file.txt"), Paths.get(""));
        return null;
    }
}
