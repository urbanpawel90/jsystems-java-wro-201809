package pl.jsystems.java1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Exercise4 {
    public static void main(String[] args) {
        // Podobnie jak w cw. 1 - odczytaj od uzytkownika
        // szereg licz, zakanczajac wczytywanie w momencie
        // wpisania 0. Liczby zbierz w kolekcji List<>
        // Na koniec wyswietl sume tych liczb.

        List<Integer> numbers = new ArrayList<>();
        Scanner sc = new Scanner(System.in);

        int lastNum, sum = 0;
        do {
            lastNum = sc.nextInt();
            if (lastNum != 0) {
                numbers.add(lastNum);
                sum += lastNum;
            }
        } while (lastNum != 0);

        for (int number : numbers) {
            System.out.println(number);
        }

        System.out.println(sum);
    }
}
