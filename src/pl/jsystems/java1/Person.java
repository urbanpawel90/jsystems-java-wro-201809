package pl.jsystems.java1;

import java.util.Objects;

public class Person {
    private String surname;
    private String name;
    private int age;

    //
    {
        System.out.println("Init");
    }

    public Person() {
        this.age = 18;
    }

    public Person(final String name, String surname, int age) {
        System.out.println("Person constructor");
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name.toUpperCase();
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void birthday() {
        this.age++;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surname, name, age);
    }
}
