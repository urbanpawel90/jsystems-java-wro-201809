package pl.jsystems.java1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Exercise1 {
    public static void main(String[] args) {
        // Odczytaj od uzytkownika ilosc pozadanych elementow
        // Odczytaj od uzytkownika te N elementow (liczby, int)
        // Wyswietl na ekranie średnią i sumę podanych liczb
        // TODO: ** Nie odczytuj ilosci na poczatku,
        //    zakoncz wczytywanie na liczbie 0 (kolekcja List<>)

        // System.in, Scanner, tablice, System.out.println()
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj ilosc liczb: ");
        int size = sc.nextInt();
        int[] numbers;
        try {
            numbers = new int[size];
        } catch (NegativeArraySizeException negativeEx) {
            System.out.println("Podales rozmiar tablicy mniejszy od 0!");
            return;
        }
        int suma = 0;

        for (int i = 0; i < size; i++) {
            System.out.println("Wpisz liczbe " + (i + 1));
            try {
                numbers[i] = sc.nextInt();
            } catch(InputMismatchException mismatchEx) {
                System.out.println("To nie liczba!");
                return;
            }

            suma += numbers[i];
        }

        System.out.println();

        System.out.println(suma);
        System.out.println((double) suma / size);
    }
}
