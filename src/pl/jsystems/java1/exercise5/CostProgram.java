package pl.jsystems.java1.exercise5;

import java.util.Calendar;

public class CostProgram {
    public static void main(String[] args) {
        Cost c1 = new OneTimeCost("TV", 50, createDate(2018, 9, 31)); // OneTime
        Cost c2 = new ReccuringCost("Internet", 100, createDate(2018, 9, 1),
                null, 30); // Reccuring

        System.out.println(c1.getDate().toInstant().toString());
        System.out.println(c2.getDate().toInstant().toString());
    }

    public static Calendar createDate(int year, int month, int day) {
        Calendar result = Calendar.getInstance();
        result.set(year, month - 1, day, 2, 0, 0);
        return result;
    }
}
