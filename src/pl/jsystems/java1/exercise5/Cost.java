package pl.jsystems.java1.exercise5;

import java.util.Calendar;

public abstract class Cost {
    private String name;
    private double amount;

    public Cost(String name, double amount) {
        this.name = name;
        this.amount = amount;
    }

    public abstract Calendar getDate();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
