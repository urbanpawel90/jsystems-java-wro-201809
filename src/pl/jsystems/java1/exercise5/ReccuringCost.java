package pl.jsystems.java1.exercise5;

import java.util.Calendar;

public class ReccuringCost extends Cost {
    private Calendar dateStart, dateEnd;
    private int period;

    public ReccuringCost(String name, double amount,
                         Calendar dateStart, Calendar dateEnd,
                         int period) {
        super(name, amount);
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.period = period;
    }

    @Override
    public Calendar getDate() {
        return dateStart;
    }

    public Calendar getDateStart() {
        return dateStart;
    }

    public void setDateStart(Calendar dateStart) {
        this.dateStart = dateStart;
    }

    public Calendar getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Calendar dateEnd) {
        this.dateEnd = dateEnd;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }
}
