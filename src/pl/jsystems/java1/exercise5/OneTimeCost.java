package pl.jsystems.java1.exercise5;

import java.util.Calendar;

public class OneTimeCost extends Cost {
    private Calendar date;

    public OneTimeCost(String name, double amount, Calendar date) {
        super(name, amount);
        this.date = date;
    }

    @Override
    public Calendar getDate() {
        return date;
    }
}
